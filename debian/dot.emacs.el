;;; dot.emacs --- sample of ~/.emacs for Goby  -*-mode: emacs-lisp;-*-

;;(setq goby-helvetica  "helvetica")
;;(setq goby-courier    "courier")
;;(setq goby-times      "times")
;;(setq goby-gothic     "gothic")
;;(setq goby-mincho     "mincho")

;; fonts-liberation
(setq goby-helvetica  "Liberation Sans")
(setq goby-courier    "Liberation Mono")
(setq goby-times      "Liberation Serif")

;; fonts-dejavu
;;(setq goby-helvetica  "DejaVu Sans")
;;(setq goby-courier    "DejaVu Sans Mono")
;;(setq goby-times      "DejaVu Serif")

;; fonts-freefont-ttf
;;(setq goby-helvetica  "FreeSans")
;;(setq goby-courier    "FreeMono")
;;(setq goby-times      "FreeSerif")

;; fonts-noto-cjk
;;(setq goby-courier    "Noto Sans Mono CJK JP")
(setq goby-gothic     "Noto Sans CJK JP")
(setq goby-mincho     "Noto Serif CJK JP")

;;(setq goby-gothic     "IPAPGothic") ;; fonts-ipafont-gothic
;;(setq goby-mincho     "IPAPMincho") ;; fonts-ipafont-mincho

;;(setq goby-gothic     "VL PGothic") ;; fonts-vlgothic
;;(setq goby-mincho     "Ume P Gothic") ;; fonts-horai-umefont

;;(setq goby-gothic     "MotoyaLMaru") ;; fonts-motoya-l-maruberi
;;(setq goby-mincho     "Monapo") ;; fonts-monapo

;;(setq goby-gothic     "Yusei Magic") ;; fonts-yusei-magic
;;(setq goby-mincho     "kiloji") ;; fonts-kiloji

;;(setq goby-helvetica  goby-gothic)

(setq goby-use-advanced-window-manager t)
;;(setq goby-use-advanced-window-manager nil)

;;(setq goby-theme 'dark)
;;(setq goby-theme 'light)

;;(setq goby-tab-spec
;;      '((0 6 "gray25"  "gray50")
;;	(1 5 "#004080" "#007080")
;;	(2 4 "#006080" "#009080")
;;	(3 3 "#008080" "#00b080")))

;;(setq goby-window-manager-top-position 0)
;;(setq goby-window-manager-left-position 0)
;;(setq goby-window-manager-view-top-position -22)
;;(setq goby-window-manager-view-left-position 0)
;;(setq goby-window-manager-bottom-margin 5)
;;(setq goby-window-manager-bottom-search-margin 3)

;;; dot.emacs ends here
