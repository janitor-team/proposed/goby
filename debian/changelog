goby (1.1+0.20180214-5) unstable; urgency=medium

  * Update examples for freefont, noto-cjk, motoya-l-maruberi, yusei-magic
  * Add fonts packages in examples to Suggests

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 08 Feb 2021 21:36:44 +0900

goby (1.1+0.20180214-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.2.1, no changes needed.
  * Remove empty maintainer scripts: None (prerm), None (postinst)

  [ Tatsuya Kinoshita ]
  * Drop debian/patches
  * Add debian/upstream/metadata
  * Add Rules-Requires-Root: no
  * Update debhelper-compat to 13
  * Update debian/watch to version 4
  * Update debian/copyright
  * Update README.Debian to use https
  * Update Standards-Version to 4.5.1

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 01 Jan 2021 16:51:26 +0900

goby (1.1+0.20180214-3) unstable; urgency=medium

  * Handle symlinks for emacsen-startup
  * Check stamp file so that source file isn't newer
  * Update Standards-Version to 4.2.0

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 22 Aug 2018 21:41:17 +0900

goby (1.1+0.20180214-2) unstable; urgency=medium

  * Accept unversioned emacs flavor for emacsen-common 3.0.0
  * Drop workaround for emacsen-common 1.x
  * Update debhelper compat version to 11
  * Update Standards-Version to 4.1.4
  * Update examples/dot.emacs.el

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 03 Jun 2018 20:24:23 +0900

goby (1.1+0.20180214-1) unstable; urgency=medium

  * New upstream version 1.1+0.20180214
  * Update 900_changelog.patch
  * Update debhelper compat version to 10
  * Prefer emacs-nox over emacs
  * Migrate from anonscm.debian.org to salsa.debian.org
  * Update debian/copyright
  * Update Standards-Version to 4.1.3
  * Update README.Debian

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 18 Feb 2018 20:22:43 +0900

goby (1.1+0.20150806-2) unstable; urgency=medium

  * Update 900_changelog.patch
  * Update debhelper compat version to 9

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 23 Oct 2016 23:55:01 +0900

goby (1.1+0.20150806-1) unstable; urgency=medium

  * New upstream version 1.1+0.20150806
  * Update debian/copyright
  * Accept emacs25
  * Update Vcs-Browser to https
  * Update Standards-Version to 3.9.8

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 23 Oct 2016 22:29:55 +0900

goby (1.1+0.20140625-1) unstable; urgency=medium

  * Imported Upstream version 1.1+0.20140625
  * Update 900_changelog.patch
  * Update debian/copyright
  * Depend on emacsen-common 2.0.8
  * Install a compat file with emacsen-compat

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 24 Aug 2014 18:35:36 +0900

goby (1.1+0.20131109-1) unstable; urgency=low

  * Imported Upstream version 1.1+0.20131109
  * New patch 900_changelog.patch to genarate ChangeLog
  * Workaround for emacsen-common <2 and debhelper <9.20131104
  * Use fonts-* instead of ttf-*
  * Add Vcs-Git and Vcs-Browser
  * Update debian/copyright
  * Update Standards-Version to 3.9.5

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 15 Dec 2013 22:11:28 +0900

goby (1.1-2) unstable; urgency=low

  * debian/control: Add emacs24 and drop emacs21
  * debian/rules: New targets build-arch and build-indep
  * debian/copyright: Update copyright-format version to 1.0
  * debian/control: Update Standards-Version to 3.9.3
  * Switch to dpkg-source 3.0 (quilt) format

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 07 Jul 2012 19:16:36 +0900

goby (1.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Update Standards-Version to 3.9.2.
  * debian/copyright: Switch to the DEP-5 format.
  * debian/README.Debian: Updated for emacs23.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 29 Jun 2011 21:55:46 +0900

goby (1.0-2) unstable; urgency=low

  * debian/control:
    - Add emacs23 and emacs-snapshot to Depends.
    - Add ${misc:Depends} to Depends.
    - Update Standards-Version to 3.8.4.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 12 Feb 2010 22:26:34 +0900

goby (1.0-1) unstable; urgency=low

  * New upstream release.
  * debian/dot.emacs.el: Add an example for ttf-umefont.
  * debian/watch: Updated.
  * debian/control:
    - Set Section to `lisp'.
    - Update Standards-Version to 3.8.2.

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 07 Jul 2009 07:32:26 +0900

goby (0.94-1) unstable; urgency=low

  * Initial Release. (closes: #502854)

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 03 Jan 2009 19:41:33 +0900
